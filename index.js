import visit from 'unist-util-visit'

const NAME = 'subheading'

export default (opts) => {
  return (root, f) => {
    visit(root, 'leaf-subheading', (node, index, parent) => {
      node.type = 'subheading'
      if (index < 1) {
        f.message('subheading should be preceded by a header', node.position,
          `${NAME}:follows-header`)
        return
      }

      const heading = parent.children[index - 1]
      if (heading.type !== 'heading') {
        f.message('subheading should be preceded by a header', node.position,
          `${NAME}:follows-header`)
        return
      }

      heading.subheading = node
    })
  }
}
